package com.mybank.core;

import com.mybank.core.model.Account;
import com.mybank.core.model.Client;
import com.mybank.core.service.AccountService;
import com.mybank.core.service.ClientService;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

@SpringBootTest
class CoreApplicationTests {

	@Autowired
	private AccountService accountService;

	@Autowired
	private ClientService clientService;

	@Test
	void contextLoads() {
	}

	@Test
	void testCreatClient_name() {
		String name = "Maya Ghalambor";
		Client client = new Client(name);
		Client savedClient = clientService.create(client);
		Assertions.assertEquals(savedClient.getName(), name);
	}

	@Test
	void testCreatClient_id() {
		String name = "Maya Ghalambor";
		Client client = new Client(name);
		Client savedClient = clientService.create(client);
		Assertions.assertNotNull(savedClient.getId());
	}

	@Test
	void testCreateAccount() {
		Account account = new Account();
		account.setClientId(1L);
		account.setBalance(BigDecimal.valueOf(1000));
		Account savedAccount = accountService.create(account);
		Assertions.assertNotNull(savedAccount.getIban());
	}
}
