package com.mybank.core.model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 *     - Account id: id of the account
 *     - Client id: client to whom the account belongs
 */

@Entity
@Table(name = "account")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Account {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 34)
    private String iban;

    @Column(name = "client_id")
    private Long clientId;

    @Column
    private BigDecimal balance;
}
