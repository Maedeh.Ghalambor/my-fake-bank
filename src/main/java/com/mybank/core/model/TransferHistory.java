package com.mybank.core.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Table(name = "transfer_history")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferHistory {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private BigDecimal transferAmount;
    @Column
    private Timestamp transferDate;

    @Column
    private Long clientId;
    @Column
    private String sourceIban;
    @Column
    private BigDecimal sourceAmountBeforeTransfer;

    @Column
    private String destinationIban;
    @Column
    private BigDecimal destinationAmountBeforeTransfer;

    /* adds more complexity
    @Column
    private Boolean status;*/
}
