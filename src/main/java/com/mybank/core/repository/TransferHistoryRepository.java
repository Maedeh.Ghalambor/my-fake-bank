package com.mybank.core.repository;

import com.mybank.core.model.TransferHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransferHistoryRepository extends JpaRepository<TransferHistory, Long> {

    List<TransferHistory> findBySourceIbanAndClientId(String sourceIban, Long clientId);

}
