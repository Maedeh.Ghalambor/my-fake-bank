package com.mybank.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class AccountExceptionHandler {

    @ExceptionHandler(AccountBusinessException.class)
    public ResponseEntity<String> exceptionHandler(AccountBusinessException exception) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        if (exception instanceof AccountBusinessException){
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        } else {
            // todo : other types for bigger projects
        }
        return new ResponseEntity<>(exception.getMessage(), status);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> overAllExceptionHandler(Exception exception) {
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        // todo : handle other exceptions
        return new ResponseEntity<>(exception.getMessage(), status);
    }
}
