package com.mybank.core.exception;

public class ClientBusinessException extends RuntimeException {
    private String message;

    public ClientBusinessException(String message) {
        super(message);
    }
}
