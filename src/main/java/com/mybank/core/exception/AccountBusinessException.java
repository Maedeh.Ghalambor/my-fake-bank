package com.mybank.core.exception;

public class AccountBusinessException extends RuntimeException {
    private String message;

    public AccountBusinessException(String message) {
        super(message);
    }
}
