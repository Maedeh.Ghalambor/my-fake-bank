package com.mybank.core.dto.mapper;

import com.mybank.core.dto.TransferHistoryResponse;
import com.mybank.core.model.TransferHistory;

import java.util.ArrayList;
import java.util.List;

public class TransferHistoryMapper {
    public static TransferHistoryResponse mapTransferHistory(TransferHistory entity) {
        TransferHistoryResponse dto = new TransferHistoryResponse();
        dto.setTransferAmount(entity.getTransferAmount());
        dto.setClientId(entity.getClientId());
        dto.setTransferDate(entity.getTransferDate());
        dto.setSourceIban(entity.getSourceIban());
        dto.setDestinationIban(entity.getDestinationIban());
        return dto;
    }

    public static List<TransferHistoryResponse> mapTransferHistoryList(List<TransferHistory> entityList) {
        List<TransferHistoryResponse> responseList = new ArrayList<>();

        entityList.forEach(entity -> {
            responseList.add(mapTransferHistory(entity));
        });

        return responseList;
    }
}
