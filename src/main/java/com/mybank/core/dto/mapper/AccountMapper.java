package com.mybank.core.dto.mapper;

import com.mybank.core.dto.AccountResponse;
import com.mybank.core.dto.CreateAccountRequest;
import com.mybank.core.model.Account;

public class AccountMapper {

    public static Account mapCreateAccountRequest(CreateAccountRequest dto) {
        Account entity = new Account();
        entity.setClientId(dto.getClientId());
        entity.setBalance(dto.getBalance());
        return entity;
    }

    public static AccountResponse mapAccountResponse(Account entity) {
        AccountResponse dto = new AccountResponse();
        if (entity != null) {
            dto.setIban(entity.getIban());
            dto.setClientId(entity.getClientId());
            dto.setBalance(entity.getBalance());
        }
        return dto;
    }

}
