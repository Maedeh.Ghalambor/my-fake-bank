package com.mybank.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferHistoryDto {
    private BigDecimal transferAmount;
    private Timestamp transferDate;
    private Long clientId;
    private String sourceIban;
    private BigDecimal sourceAmountBeforeTransfer;
    private String destinationIban;
    private BigDecimal destinationAmountBeforeTransfer;
}
