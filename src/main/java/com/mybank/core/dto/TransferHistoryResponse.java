package com.mybank.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferHistoryResponse {
    private BigDecimal transferAmount;
    private Timestamp transferDate;
    private Long clientId;
    private String sourceIban;
    private String destinationIban;
}
