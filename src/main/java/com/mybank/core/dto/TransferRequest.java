package com.mybank.core.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TransferRequest {
    @NotEmpty
    private String sourceIban;

    @NotEmpty
    private String destinationIban;

    @NotNull
    private Long clientId;

    @NotNull
    private BigDecimal amount;
}
