package com.mybank.core.service;

import com.mybank.core.dto.TransferHistoryDto;
import com.mybank.core.model.Account;
import com.mybank.core.model.TransferHistory;

import java.util.List;

public interface TransferHistoryService {

    TransferHistory save(TransferHistoryDto dto);

    List<TransferHistory> read(Account account);
}
