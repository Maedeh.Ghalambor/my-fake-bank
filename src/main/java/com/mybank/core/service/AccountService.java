package com.mybank.core.service;

import com.mybank.core.dto.TransferRequest;
import com.mybank.core.exception.AccountBusinessException;
import com.mybank.core.model.Account;
import com.mybank.core.model.TransferHistory;

import java.util.List;

public interface AccountService {
    Account create(Account account);

    Account read(String iban) throws AccountBusinessException;

    Account transfer(TransferRequest request) throws AccountBusinessException;

    List<TransferHistory> retrieveTransferHistoryAccount(String iban, Long clientId);
}
