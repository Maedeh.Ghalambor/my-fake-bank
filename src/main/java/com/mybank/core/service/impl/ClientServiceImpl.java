package com.mybank.core.service.impl;

import com.mybank.core.exception.AccountBusinessException;
import com.mybank.core.exception.ClientBusinessException;
import com.mybank.core.model.Account;
import com.mybank.core.model.Client;
import com.mybank.core.repository.ClientRepository;
import com.mybank.core.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Override
    public Client create(Client client) {
        return clientRepository.save(client);
    }

    public Client read(Long clientId) {
        return clientRepository.findById(clientId)
                .orElseThrow(() -> new ClientBusinessException("Client Not Found"));
    }

    @Override
    public void validateAccount(Long clientId, String iban) {
        Client client = read(clientId);
        if (client.getAccounts().stream().map(Account::getIban).anyMatch(it -> it.equals(iban))) {
            throw new AccountBusinessException("This Client already has this account");
        }
    }
}
