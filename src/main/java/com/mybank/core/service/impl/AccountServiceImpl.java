package com.mybank.core.service.impl;

import com.mybank.core.dto.TransferHistoryDto;
import com.mybank.core.dto.TransferRequest;
import com.mybank.core.exception.AccountBusinessException;
import com.mybank.core.model.Account;
import com.mybank.core.model.TransferHistory;
import com.mybank.core.repository.AccountRepository;
import com.mybank.core.service.AccountService;
import com.mybank.core.service.ClientService;
import com.mybank.core.service.TransferHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Random;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final ClientService clientService;
    private final TransferHistoryService transferHistoryService;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, ClientService clientService, TransferHistoryService transferHistoryService) {
        this.accountRepository = accountRepository;
        this.clientService = clientService;
        this.transferHistoryService = transferHistoryService;
    }

    @Override
    public Account create(Account account) {
        // todo : iban algorithm?!
        String iban = account.getClientId() + "-123-"+ new Random().nextInt(100);
        account.setIban(iban);
        clientService.validateAccount(account.getClientId(), account.getIban());
        return accountRepository.save(account);
    }

    @Override
    public Account read(String iban) throws AccountBusinessException {
        return accountRepository.findByIban(iban)
                .orElseThrow(() -> new AccountBusinessException("No Account found with this iban number"));
    }

    @Override
    public Account transfer(TransferRequest request) throws AccountBusinessException {
        Account source = read(request.getSourceIban());

        validateRetrieveInformationOnAccount(source, request.getClientId());

        BigDecimal amount = request.getAmount();
        if (source.getBalance().compareTo(amount) <= 0) {
            throw new AccountBusinessException("Not enough money in the source account");
        }
        Account destination = read(request.getDestinationIban());

        // keep the history of transfers
        TransferHistoryDto transferHistoryDto = new TransferHistoryDto();
        transferHistoryDto.setTransferAmount(amount);
        transferHistoryDto.setTransferDate(new Timestamp(System.currentTimeMillis()));
        transferHistoryDto.setClientId(source.getClientId());
        transferHistoryDto.setSourceIban(source.getIban());
        transferHistoryDto.setSourceAmountBeforeTransfer(source.getBalance());
        transferHistoryDto.setDestinationIban(source.getIban());
        transferHistoryDto.setDestinationAmountBeforeTransfer(destination.getBalance());

        source.setBalance(source.getBalance().subtract(amount));
        destination.setBalance(destination.getBalance().add(amount));

        accountRepository.save(destination);
        accountRepository.save(source);
        transferHistoryService.save(transferHistoryDto);

        return source;
    }

    @Override
    public List<TransferHistory> retrieveTransferHistoryAccount(String iban, Long clientId) {
        // handles if the wrong iban has been entered
        Account account = read(iban);
        validateRetrieveInformationOnAccount(account, clientId);
        return transferHistoryService.read(account);
    }


    private void validateRetrieveInformationOnAccount(Account account, Long clientId) throws AccountBusinessException {
        if (!account.getClientId().equals(clientId)) {
            throw new AccountBusinessException("Access Denied for this client");
        }
    }
}
