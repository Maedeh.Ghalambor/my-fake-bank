package com.mybank.core.service.impl;

import com.mybank.core.dto.TransferHistoryDto;
import com.mybank.core.model.Account;
import com.mybank.core.model.TransferHistory;
import com.mybank.core.repository.TransferHistoryRepository;
import com.mybank.core.service.TransferHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

@Service
public class TransferHistoryServiceImpl implements TransferHistoryService {

    private final TransferHistoryRepository transferHistoryRepository;

    @Autowired
    public TransferHistoryServiceImpl(TransferHistoryRepository transferHistoryRepository) {
        this.transferHistoryRepository = transferHistoryRepository;
    }

    @Override
    public TransferHistory save(TransferHistoryDto dto) {
        TransferHistory transferHistory = new TransferHistory();
        transferHistory.setTransferDate(new Timestamp(System.currentTimeMillis()));
        transferHistory.setTransferAmount(dto.getTransferAmount());
        transferHistory.setClientId(dto.getClientId());
        transferHistory.setSourceIban(dto.getSourceIban());
        transferHistory.setSourceAmountBeforeTransfer(dto.getSourceAmountBeforeTransfer());
        transferHistory.setDestinationIban(dto.getDestinationIban());
        transferHistory.setDestinationAmountBeforeTransfer(dto.getDestinationAmountBeforeTransfer());
        return transferHistoryRepository.save(transferHistory);
    }

    @Override
    public List<TransferHistory> read(Account account) {
        return transferHistoryRepository.findBySourceIbanAndClientId(account.getIban(), account.getClientId());
    }
}
