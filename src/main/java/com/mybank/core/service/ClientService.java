package com.mybank.core.service;

import com.mybank.core.model.Client;

public interface ClientService {
    Client create(Client client);

    void validateAccount(Long clientId, String iban);
}
