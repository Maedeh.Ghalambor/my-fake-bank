package com.mybank.core.controller;

import com.mybank.core.dto.*;
import com.mybank.core.dto.mapper.AccountMapper;
import com.mybank.core.dto.mapper.TransferHistoryMapper;
import com.mybank.core.model.Account;
import com.mybank.core.model.TransferHistory;
import com.mybank.core.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public ResponseEntity<AccountResponse> create(@Valid CreateAccountRequest request) {
        Account entity = accountService.create(AccountMapper.mapCreateAccountRequest(request));
        AccountResponse response = AccountMapper.mapAccountResponse(entity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{iban}")
    public ResponseEntity<AccountResponse> read(@PathVariable("iban") String iban) {
        Account entity = accountService.read(iban);
        AccountResponse response = AccountMapper.mapAccountResponse(entity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<AccountResponse> readBalance(@NotNull @RequestParam(required = true) String iban) {
        Account entity = accountService.read(iban);
        AccountResponse response = AccountMapper.mapAccountResponse(entity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping("/transfer")
    public ResponseEntity<AccountResponse> transfer(@Valid TransferRequest request) {
        Account sourceAccount = accountService.transfer(request);
        AccountResponse response = AccountMapper.mapAccountResponse(sourceAccount);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/transfer/history")
    public ResponseEntity<List<TransferHistoryResponse>> retrieveTransferHistory(TransferHistoryRequest request) {
        List<TransferHistory> transferHistoryList = accountService.retrieveTransferHistoryAccount(request.getAccountIban(), request.getClientId());
        List<TransferHistoryResponse> responseList = TransferHistoryMapper.mapTransferHistoryList(transferHistoryList);
        return new ResponseEntity<>(responseList, HttpStatus.OK);
    }
}
