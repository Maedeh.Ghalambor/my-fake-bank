package com.mybank.core.controller;

import com.mybank.core.dto.CreateClientResponse;
import com.mybank.core.model.Client;
import com.mybank.core.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/clients")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    public ResponseEntity<CreateClientResponse> create(@RequestParam @NotEmpty String clientName) {
        Client entity = clientService.create(new Client(clientName));
        CreateClientResponse response = new CreateClientResponse(entity.getId(), entity.getName());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
